FROM node:12-alpine as build

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile 

COPY . .
RUN yarn build

FROM node:12-alpine

WORKDIR /app

COPY --from=build /app/build build
COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile --production=true

CMD [ "node", "./build/app.js" ]


