import express from 'express'
import apiRouter from './api/routes'

const port = 8000
const app = express()


app.use('/api', apiRouter)
const server = app.listen(port, "0.0.0.0", () => { console.log(`Starting server on port ${port}`) })

export default server