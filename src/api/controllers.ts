import { Request, Response } from 'express'
import CacheClient from '../services/cache'
import SpendingReport from '../services/spendingReport'

const buildCacheKey = (userId: string, from: Date, to: Date) => (
  `users/${userId}/spending-report/${from.toISOString()}/${to.toISOString()}`
)

export const getUserSpendingReport = async (request: Request, response: Response): Promise<void> => {
  const fromDate = new Date(request.query.from as string);
  const toDate = new Date(request.query.to as string);
  const userId = request.params.userId

  if (isNaN(fromDate.getTime()) || isNaN(toDate.getTime())) {
    response.status(400).send({ 'message': 'Please specify query parameters "from" and "to" in an ISO date format' })
    return
  }
  const cache = new CacheClient()
  const cacheKey = buildCacheKey(userId, fromDate, toDate)

  const cachedData = await cache.get(cacheKey)

  if (cachedData) {
    response.json(JSON.parse(cachedData))
    return
  }

  const reportService = new SpendingReport(userId)
  try {
    const reportData = await reportService.generateReport(fromDate, toDate)
    cache.set(cacheKey, JSON.stringify(reportData))

    response.json(reportData)
  } catch (e) {
    response.status(500).send({ 'message': 'An error occurred while trying to generate the report' })
  }
}