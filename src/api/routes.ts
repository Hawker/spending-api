import express from 'express'
import { getUserSpendingReport } from './controllers'

const router = express.Router()

router.get('/users/:userId/spending-report', getUserSpendingReport)

export default router