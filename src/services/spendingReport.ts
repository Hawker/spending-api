import DbClient from './db'

interface SpendingByMerchantRow {
  merchantId: number | string,
  totalSpent: number,
}

interface SpendingByMerchantNameRow extends SpendingByMerchantRow {
  merchantName: string,
}


export default class SpendingReport {
  userId: number | string
  dbClient: DbClient

  constructor(userId: number | string) {
    this.userId = userId
    this.dbClient = new DbClient()
  }

  async getUserTotalSpendingByMerchant(from: Date, to: Date): Promise<SpendingByMerchantNameRow[]> {
    try {
      return await this.dbClient.executeQuery(
        `SELECT m.display_name AS merchantName, m.id AS merchantId, SUM(t.amount) AS totalSpent
      FROM transaction AS t
      LEFT JOIN merchant m ON t.merchant_id=m.id
      WHERE (t.date > '${from.toISOString()}' AND t.date < '${to.toISOString()}')
      AND t.user_id = ?
      GROUP BY t.merchant_id, t.user_id 
      ORDER BY t.merchant_id, totalSpent DESC`,
        [this.userId]
      )
    } catch (e) {
      return []
    }
  }

  async getOtherUsersTotalSpendingByMerchant(from: Date, to: Date, merchantIds: (number | string)[]): Promise<SpendingByMerchantRow[]> {
    try {
      return await this.dbClient.executeQuery(
        `SELECT t.merchant_id AS merchantId, SUM(t.amount) AS totalSpent
        FROM transaction AS t
        WHERE (t.date > '${from.toISOString()}' AND t.date < '${to.toISOString()}')
        AND t.user_id != ?
        AND t.merchant_id IN (?)
        GROUP BY t.merchant_id, t.user_id 
        ORDER BY t.merchant_id, totalSpent DESC`,
        [this.userId, merchantIds]
      )
    } catch (e) {
      return []
    }
  }

  async generateReport(from: Date, to: Date) {
    const spendingByMerchantRows = await this.getUserTotalSpendingByMerchant(from, to)

    if (!spendingByMerchantRows.length)
      return []

    const merchantIds = spendingByMerchantRows.map(row => row.merchantId)

    const otherUsersSpendingRows = await this.getOtherUsersTotalSpendingByMerchant(from, to, merchantIds)

    if (!otherUsersSpendingRows.length)
      return spendingByMerchantRows.map(row => ({ merchantName: row.merchantName, percentileRank: 0 }))

    const otherUsersSpendingByMerchantId = otherUsersSpendingRows.reduce((prev, current) => {
      if (!prev[current.merchantId])
        prev[current.merchantId] = [current.totalSpent]
      else
        prev[current.merchantId].push(current.totalSpent)
      return prev
    }, {} as { [key: string]: [number] })

    let results = []
    for (const merchantId in otherUsersSpendingByMerchantId) {
      const totals = otherUsersSpendingByMerchantId[merchantId]
      const userSpending = spendingByMerchantRows.find(x => x.merchantId == merchantId) as SpendingByMerchantNameRow
      const count = totals.reduce((total, current) => (total + (userSpending.totalSpent > current ? 1 : 0)), 0)

      const percentileRank = (count / totals.length)
      results.push({
        merchantName: userSpending.merchantName,
        percentileRank: Number(percentileRank.toFixed(2))
      })
    }
    return results
  }
}
