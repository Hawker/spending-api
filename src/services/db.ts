import mysql, { Connection, MysqlError } from 'mysql'

export default class DbClient {
  connectionString: string

  constructor() {
    this.connectionString = process.env.DB_CONNECTION_STRING as string
  }

  getConnection() {
    return mysql.createConnection(this.connectionString)
  }

  executeQuery(query: string, inputs: any): Promise<[]> {
    return new Promise((resolve, reject) => {
      const connection = this.getConnection()
      connection.query(query, inputs, (error: any, results: [], fields: any) => {
        connection.destroy()
        if (error)
          reject(error)
        resolve(results)
      })
    })
  }
}
