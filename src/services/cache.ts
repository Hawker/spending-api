import redis, { RedisClient } from 'redis'

export default class CacheClient {
  client: RedisClient

  constructor() {
    this.client = redis.createClient(process.env.REDIS_CONNECTION_STRING as string)
  }

  get(key: string): Promise<string | null> {
    return new Promise<string | null>((resolve, reject) => {
      this.client.get(key, (error, data) => {
        if (error)
          reject(error)

        resolve(data)
      })
    })
  }

  set(key: string, value: any, expirySeconds: number = 3600): boolean {
    return this.client.setex(key, expirySeconds, value)
  }

}