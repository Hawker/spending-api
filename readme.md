Docker
------

To run the application:
docker-compose up -d app

To run the tests:
docker-compose run app-tests


Local
-----

To run the application:
yarn install
yarn start

To run the tests:
yarn install
yarn test