import request from 'supertest'
import app from '../../src/app'
import SpendingReport from '../../src/services/spendingReport'

jest.mock('../../src/services/cache')
jest.mock('../../src/services/db')

const spendingByMerchant = [
  { merchantId: 1, merchantName: 'a', totalSpent: 30 },
  { merchantId: 2, merchantName: 'b', totalSpent: 20 },
  { merchantId: 3, merchantName: 'c', totalSpent: 10 },
]

describe('GET /users/{id}/spending-report', () => {
  beforeEach(() => {

  })

  afterEach(() => {
    jest.restoreAllMocks()
  })

  it('generates a spending report', async () => {
    const mockGetUserTotalSpendingByMerchant = jest.spyOn(
      SpendingReport.prototype, 'getUserTotalSpendingByMerchant'
    ).mockImplementation(async () => spendingByMerchant)

    const otherUsersSpendingByMerchant = [
      { merchantId: 1, totalSpent: 5 },
      { merchantId: 1, totalSpent: 40 },
      { merchantId: 2, totalSpent: 20 },
      { merchantId: 2, totalSpent: 3 },
      { merchantId: 2, totalSpent: 35 },
      { merchantId: 3, totalSpent: 20 },
      { merchantId: 3, totalSpent: 30 },
      { merchantId: 3, totalSpent: 50 },
    ]

    const mockGetOtherUsersTotalSpendingByMerchant = jest.spyOn(
      SpendingReport.prototype, 'getOtherUsersTotalSpendingByMerchant'
    ).mockImplementation(async () => otherUsersSpendingByMerchant)

    const res = await request(app)
      .get('/api/users/1/spending-report?from=2020-09-01&to=2020-09-20')
      .expect(200, [
        { merchantName: 'a', percentileRank: 0.5 },
        { merchantName: 'b', percentileRank: 0.33 },
        { merchantName: 'c', percentileRank: 0 },
      ])

    const fromDate = new Date('2020-09-01')
    const toDate = new Date('2020-09-20')

    expect(mockGetUserTotalSpendingByMerchant).toBeCalledWith(fromDate, toDate)
    expect(mockGetOtherUsersTotalSpendingByMerchant).toBeCalledWith(fromDate, toDate, [1, 2, 3])
  })

  it('generates an empty report when the user has not done any spending', async () => {
    const mockGetUserTotalSpendingByMerchant = jest.spyOn(
      SpendingReport.prototype, 'getUserTotalSpendingByMerchant'
    ).mockImplementation(async () => [])

    const res = await request(app)
      .get('/api/users/1/spending-report?from=2020-09-01&to=2020-09-20')
      .expect(200, [])

    const fromDate = new Date('2020-09-01')
    const toDate = new Date('2020-09-20')
    expect(mockGetUserTotalSpendingByMerchant).toBeCalledWith(fromDate, toDate)
  })

  it('generates spending report when other users have not done any spending', async () => {
    jest.spyOn(SpendingReport.prototype, 'getUserTotalSpendingByMerchant').mockImplementation(async () => spendingByMerchant)
    jest.spyOn(SpendingReport.prototype, 'getOtherUsersTotalSpendingByMerchant').mockImplementation(async () => [])

    const res = await request(app)
      .get('/api/users/1/spending-report?from=2020-09-01&to=2020-09-20')
      .expect(200, [
        { merchantName: 'a', percentileRank: 0 },
        { merchantName: 'b', percentileRank: 0 },
        { merchantName: 'c', percentileRank: 0 },
      ])
  })

  it('returns a 400 when invalid date values specified', async () => {
    const res = await request(app)
      .get('/api/users/1/spending-report?from=adad&to=adsad')
      .expect(400)
  })

  it('returns a 400 when no dates specfied', async () => {
    const res = await request(app)
      .get('/api/users/1/spending-report')
      .expect(400)
  })
})